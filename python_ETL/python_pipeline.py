from helper_files.extract_functions import spark_initializer, load_CSV, Load_pubmed
from helper_files.transform_functions import normalize_pubmed_df, normalize_df, vertex_processing, edge_processing
from helper_files.load_functions import save_to_json
from helper_files.variables import clinical_path, dict_paths, dict_granularity_clinical_trial, drugs_path, pubmedJson_path, pubmedCsv_path, dict_granularity_pubmed, dict_column, dict_type, output_path


def main():
    spark, sc = spark_initializer('Servier_test_ETL')
    log4jLogger = sc._jvm.org.apache.log4j
    LOGGER = log4jLogger.LogManager.getLogger(__name__)

    LOGGER.info("pyspark script logger initialized")
    
    LOGGER.info("Extrating Clinical Data from CSV")

    df_clinical = load_CSV(spark = spark
                            , path_csv_input = clinical_path
                            , path_csv_output = dict_paths['path_local_clinical' ] 
                            ,dict_gran = dict_granularity_clinical_trial
                            )

    LOGGER.info("Extrating Drug Data from CSV")
    df_drug = load_CSV(spark = spark
                        , path_csv_input = drugs_path
                        , path_csv_output = dict_paths['path_local_drug' ] 
                        ,formDate = False
                        )

    
    LOGGER.info("Extrating Pubmed Data from both CSV and JSON")
    df_pubmed = Load_pubmed(spark = spark, sc = sc
                        ,path_json_input = pubmedJson_path
                        ,path_csv_input = pubmedCsv_path
                        ,path_csv_output = dict_paths['path_local_pubmed' ]
                        ,formDate = True
                        ,dict_gran = dict_granularity_pubmed
                        )

    
    LOGGER.info("Normalizing names of all columns accross dataframes")
    df_pubmed_norm = normalize_pubmed_df(spark = spark
                                            , spark_df_input_path = dict_paths['path_local_pubmed' ]
                                            , spark_df_output_path = dict_paths['path_local_pubmed_norm' ]
                                            , dict_rename = dict_column['pubmed']
                                        )
    
    df_clinical_norm = normalize_df(spark = spark
                                            , spark_df_input_path = dict_paths['path_local_clinical' ]
                                            , spark_df_output_path = dict_paths['path_local_clinical_norm' ]
                                            , dict_rename = dict_column['clinical']
                                        )
    df_drug_norm = normalize_df(spark = spark
                                            , spark_df_input_path = dict_paths['path_local_drug' ]
                                            , spark_df_output_path = dict_paths['path_local_drug_norm' ]
                                            , dict_rename = dict_column['drug']
                                        )


    LOGGER.info("Creating vertex data for graph representation")
    df_vertex = vertex_processing (spark = spark
                                    , df_drugs_path = dict_paths['path_local_drug_norm' ]
                                    , df_pubmed_path = dict_paths['path_local_pubmed_norm' ]
                                    , df_clinical_path = dict_paths['path_local_clinical_norm' ]
                                    , spark_df_output_path = dict_paths['path_local_vertex' ]
                                    , dict_type = dict_type
                                    , dict_column = dict_column
                                    )

    LOGGER.info("Creating edge data for graph representation")
    df_edge = edge_processing (spark = spark
                                    , df_drugs_path = dict_paths['path_local_drug_norm' ]
                                    , df_pubmed_path = dict_paths['path_local_pubmed_norm' ]
                                    , df_clinical_path = dict_paths['path_local_clinical_norm' ]
                                    , spark_df_output_path = dict_paths['path_local_edge' ]
                                    , dict_column = dict_column )


    LOGGER.info("Saving Graph representation to JSON")
    save_to_json(spark, dict_paths['path_local_vertex' ], dict_paths['path_local_edge' ], output_path)

    
    LOGGER.info("Success")
    return(0)
if __name__ == "__main__":
    # execute only if run as a script
    main()
