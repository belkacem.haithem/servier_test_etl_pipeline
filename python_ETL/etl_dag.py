import airflow
from airflow import DAG
from airflow.operators.python import PythonOperator
from helper_files.extract_functions import spark_initializer, load_CSV, Load_pubmed
from helper_files.transform_functions import normalize_pubmed_df, normalize_df, vertex_processing, edge_processing
from helper_files.load_functions import save_to_json
from helper_files.variables import dict_paths, clinical_path, dict_granularity_clinical_trial, drugs_path, pubmedJson_path, pubmedCsv_path, dict_granularity_pubmed, dict_column, dict_type, output_path


default_args = {
    'owner': 'flolas',
    'depends_on_past': True,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['belkacem.haithem@gmail.com'],
    'email_on_failure': False,
    'email_on_retry': False,

    }

spark, sc = spark_initializer('Servier_test_ETL')

dag = DAG('etl-test-servier'
            , default_args=default_args
            , schedule_interval="@daily")



t1_1 = PythonOperator(
        task_id='import_clinical',
        python_callable=load_CSV,
        op_kwargs={
                       "spark" : spark
                        , "path_csv_input" : clinical_path
                        , "path_csv_output" : dict_paths['path_local_clinical'] 
                        , "dict_gran" : dict_granularity_clinical_trial
         },
        dag=dag)

t1_2 = PythonOperator(
        task_id='import_drug',
        python_callable=load_CSV,
        op_kwargs={
                       "spark" : spark
                        , "path_csv_input" : drugs_path
                        , "path_csv_output" : dict_paths['path_local_drug'] 
                        , "formDate" : False
        },
        dag=dag)


t1_3 = PythonOperator(
        task_id='import_pubmed',
        python_callable=Load_pubmed,
        op_kwargs={
                       "spark" : spark
                        , "sc" : sc
                        , "path_json_input" : pubmedJson_path
                        , "path_csv_input" : pubmedCsv_path
                        , "path_csv_output" : dict_paths['path_local_pubmed' ]
                        , "formDate" : True
                        , "dict_gran" : dict_granularity_pubmed
        },
        dag=dag)



t2_1 = PythonOperator(
        task_id='norm_pubmed',
        python_callable=normalize_pubmed_df,
        op_kwargs={
                        "spark" : spark
                        , "spark_df_input_path" : dict_paths['path_local_pubmed' ]
                        , "spark_df_output_path" : dict_paths['path_local_pubmed_norm' ]
                        , "dict_rename" : dict_column['pubmed']
        },
        dag=dag)

t2_2 = PythonOperator(
        task_id='norm_drug',
        python_callable=normalize_df,
        op_kwargs={
                        "spark" : spark
                        , "spark_df_input_path" : dict_paths['path_local_drug' ]
                        , "spark_df_output_path" : dict_paths['path_local_drug_norm' ]
                        , "dict_rename" : dict_column['drug']
        },
        dag=dag)

t2_3 = PythonOperator(
        task_id='norm_clinical',
        python_callable=normalize_df,
        op_kwargs={
                        "spark" : spark
                        , "spark_df_input_path" : dict_paths['path_local_clinical' ]
                        , "spark_df_output_path" : dict_paths['path_local_clinical_norm' ]
                        , "dict_rename" : dict_column['clinical']
        },
        dag=dag)

t3_1 = PythonOperator(
        task_id='vertex_processing',
        python_callable=vertex_processing,
        op_kwargs={
                        "spark" : spark
                        , "df_drugs_path" : dict_paths['path_local_drug_norm' ]
                        , "df_pubmed_path" : dict_paths['path_local_pubmed_norm' ]
                        , "df_clinical_path" : dict_paths['path_local_clinical_norm' ]
                        , "spark_df_output_path" : dict_paths['path_local_vertex' ]
                        , "dict_type" : dict_type
                        , "dict_column" : dict_column
        },
        dag=dag)


t3_2 = PythonOperator(
        task_id='edge_processing',
        python_callable=edge_processing,
        op_kwargs={
                        "spark" : spark
                        , "df_drugs_path" : dict_paths['path_local_drug_norm' ]
                        , "df_pubmed_path" : dict_paths['path_local_pubmed_norm' ]
                        , "df_clinical_path" : dict_paths['path_local_clinical_norm' ]
                        , "spark_df_output_path" : dict_paths['path_local_edge' ]
                        , "dict_column" : dict_column
        },
        dag=dag)

t4 = PythonOperator(
        task_id='End',
        python_callable=save_to_json,
        op_kwargs={
                        "spark" : spark
                        , "df_vertex_path" : dict_paths['path_local_vertex' ]
                        , "df_edge_path" : dict_paths['path_local_edge' ]
                        , "output_path" : output_path
        },
        dag=dag)

t1_1 >> [t2_1, t2_2, t2_3]
t1_2 >> [t2_1, t2_2, t2_3]
t1_3 >> [t2_1, t2_2, t2_3]
t2_1 >> [t3_1, t3_2]
t2_2 >> [t3_1, t3_2]
t2_3 >> [t3_1, t3_2]
t3_1 >> t4
t3_2 >> t4
