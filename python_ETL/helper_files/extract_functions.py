from pyspark import SparkContext
from pyspark.sql import SparkSession,  Window
from pyspark.sql.functions import isnan, when, count, col, to_date, regexp_replace, last
from helper_files.variables import pubmedJson_path, pubmedCsv_path, dict_granularity_pubmed
import re
import json
import sys

###########################################

def spark_initializer(Name):
    """
    Initializes spark environment
    """
    sc = SparkContext()
    spark = SparkSession \
        .builder \
        .appName(Name) \
        .getOrCreate()
    return(spark, sc)

def count_missings(spark_df,sort=True):
    """
    Counts number of nulls and empty strings in each column and can sort them
    """
    df = spark_df.select([count(when(regexp_replace(col(c), " ", "") == '', c).when((isnan(c)) | (col(c).isNull()), c)).alias(c) for (c,c_type) in spark_df.dtypes if c_type not in ('timestamp', 'date', 'int')]).toPandas()
    
    if len(df) == 0:
        print("There are no any missing values!")
        return None

    if sort:
        return df.rename(index={0: 'count'}).T.sort_values("count",ascending=False)

    return df


def format_date (df_spark, input_column = 'date',  output_column = 'date'):
    """Format Date columns with several cases
        the name of the input and output columns are arguments (default = date, date)
    """
    df_output = df_spark.withColumn(output_column, 
        when(to_date(col(input_column),"yyyy-MM-dd").isNotNull(),
            to_date(col(input_column),"yyyy-MM-dd"))
        .when(to_date(col(input_column),"yyyy MM dd").isNotNull(),
            to_date(col(input_column),"yyyy MM dd"))
        .when(to_date(col(input_column),"MM/dd/yyyy").isNotNull(),
            to_date(col(input_column),"MM/dd/yyyy"))
        .when(to_date(col(input_column),"dd/MM/yyyy").isNotNull(),
            to_date(col(input_column),"dd/MM/yyyy"))
        .when(to_date(col(input_column),"d MMMM yyyy").isNotNull(),
            to_date(col(input_column),"d MMMM yyyy"))
        .otherwise('Unknown Format')
    )
    return df_output


def ffil_from_primary_key(df_input, dict_dependance):
    """ Function that allows to forward fill rows using granularities and scales
        For example if the id is the primary key of the table we fill all the other rows accordingly to fill null values
            Then if the scientific Title is the second key it will allow to fill only dates and journals
    """
    df_output = df_input
    for key in list(dict_dependance.keys()):
        for column in dict_dependance[key]:
            if column != key:
                window = Window.partitionBy(key)\
                            .orderBy((col(column).desc()))\
                            .rowsBetween(-sys.maxsize, 0)
                filled_column = last(df_output[column], ignorenulls=True).over(window)
                df_output = df_output.withColumn(column, filled_column)
    return(df_output)

def rename_col(spark_df_input, dict_rename):
    """Renames the columns of a spark dataframe using key / value couples from a dictionnary

    """
    spark_df_output = spark_df_input
    for col in spark_df_input.columns:
        spark_df_output = spark_df_output.withColumnRenamed(col, dict_rename[col])
    return (spark_df_output)

def load_CSV(spark, path_csv_input, path_csv_output, formDate = True, dict_gran = False):
    """Loads data from CSV files,
    format dates,
    replaces empty strings by Null values
    """
    df_loaded = spark.read.csv(
                                path_csv_input, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )
    df_loaded = df_loaded.select(*(when(regexp_replace(col(c), " ", "") != '', col(c)).otherwise(None).alias(c) for c in df_loaded.columns)) 
    if formDate:
        df_loaded = format_date(df_loaded)
    
    if dict_gran:
        df_loaded = ffil_from_primary_key(df_loaded, dict_gran)
    df_loaded.write.mode('overwrite').csv(path_csv_output, header = 'true')
    return(df_loaded)



def load_json(spark, sc, path_json_input, path_json_output, formDate = True, dict_gran = False):
    """Loads data from a json file 
    Json files may contain trailing commas necessiting some specific processing (regex to remove the comma)
    Some additional processing is needed to format data to date format.

    """
    with open(path_json_input, "r") as file:
        json_pubmed = file.read()
    regex = r'''(?<=[}\]"']),(?!\s*[{["'])'''
    json_pubmed = re.sub(regex, "", json_pubmed, 0)
    df_loaded = spark.read.json(sc.parallelize([json_pubmed]))
    df_loaded = format_date (df_loaded)
    df_loaded = df_loaded.select(*(when(regexp_replace(col(c), " ", "") != '', col(c)).otherwise(None).alias(c) for c in df_loaded.columns)) 
    if formDate:
        df_loaded = format_date(df_loaded)
    
    if dict_gran:
        df_loaded.write.mode('append').csv(path_json_output, header = 'true')
    return(df_loaded)



def Load_pubmed (spark, sc, 
                    path_json_input, 
                    path_csv_input, 
                    path_csv_output,
                    formDate = True, 
                    dict_gran = dict_granularity_pubmed):
    """
    Loads Pubmed files from both Json and CSV data
    """
    df_pubmed_csv = load_CSV(spark, path_csv_input, path_csv_output, formDate = formDate, dict_gran = dict_gran)
    df_pubmed_json = load_json(spark, sc, path_json_input, path_csv_output, formDate = formDate, dict_gran = dict_gran)


    df_loaded = df_pubmed_csv.unionByName(df_pubmed_json)
    
    df_loaded.write.mode('overwrite').csv(path_csv_output, header = 'true')
    return(path_csv_output)





