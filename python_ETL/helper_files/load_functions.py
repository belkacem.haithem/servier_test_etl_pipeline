import json

def save_to_json(spark, df_vertex_path, df_edge_path, output_path):

    df_vertex = spark.read.csv(
                                df_vertex_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )
    
    df_edge = spark.read.csv(
                                df_edge_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )

    a = df_vertex.toJSON().collect()
    b = df_edge.toJSON().collect()
    with open(output_path, 'w') as f:
        json.dump({"vertex" : a, "edge" : b}, f)