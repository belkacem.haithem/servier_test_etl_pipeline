from pyspark.sql.functions import col, lower, sha1, lit
from helper_files.variables import dict_column
from helper_files.extract_functions import rename_col

def normalize_pubmed_df(spark, spark_df_input_path, spark_df_output_path, dict_rename = dict_column['pubmed']):
    """ Renames columns of the pubmed spark dataframe and create a new id column using the sha1 of title
    """
    spark_df_output = spark.read.csv(
                                spark_df_input_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )
    cols = spark_df_output.columns
    spark_df_output = spark_df_output.withColumn(cols[0], sha1(col(cols[1])))
    spark_df_output = rename_col(spark_df_output, dict_rename)
    spark_df_output.write.mode('overwrite').csv(spark_df_output_path, header = 'true')
    return(spark_df_output_path)

def normalize_df(spark, spark_df_input_path, spark_df_output_path, dict_rename):
    """ Renames columns of dataframes
    """

    spark_df_output = spark.read.csv(
                                spark_df_input_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )
    
    cols = spark_df_output.columns
    spark_df_output = rename_col(spark_df_output, dict_rename)
    spark_df_output.write.mode('overwrite').csv(spark_df_output_path, header = 'true')
    return(spark_df_output_path)


def vertex_processing (spark, df_drugs_path, df_pubmed_path, df_clinical_path, spark_df_output_path, dict_type, dict_column ):
    """ Outputs the vertex view of the graph modelling
        (ID, Name, Type) of all entities ( drugs, article, journal....)
    """
    df_drugs = spark.read.csv(
                                df_drugs_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )
    
    df_pubmed = spark.read.csv(
                                df_pubmed_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )
    
    df_clinical = spark.read.csv(
                                df_clinical_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )


    vertex_df_drugs = df_drugs.withColumn(dict_type["type"], lit(dict_type['drug']))

    dict_col = dict_column['pubmed']
    vertex_df_pubmed = df_pubmed.select(dict_col["id"], dict_col["title"])\
                                 .withColumn(dict_type["type"], lit(dict_type['pubmed']))
    

    dict_col = dict_column['clinical']
    vertex_df_clinical = df_clinical.select(dict_col["id"], dict_col["scientific_title"])\
                                 .withColumn(dict_type["type"], lit(dict_type['clinical']))

    
    vertex_df_journal = df_clinical.select("journal")\
                        .union(df_pubmed.select("journal"))\
                        .distinct()\
                        .withColumn("id", sha1("journal"))\
                        .withColumn(dict_type["type"], lit(dict_type['journal']))\
                        .select("id", "journal", dict_type["type"])
    
    vertex_df = vertex_df_pubmed\
                .union(vertex_df_drugs)\
                .union(vertex_df_clinical)\
                .union(vertex_df_journal)


    vertex_df.write.mode('overwrite').csv(spark_df_output_path, header = 'true')
    return(spark_df_output_path)


def edge_processing (spark, df_drugs_path, df_pubmed_path, df_clinical_path, spark_df_output_path, dict_column):

    """ Outputs the edge view of the graph modelling
        (src, dst, date)  
        src is the ID of a drug, 
        dst is the ID of the entity the drug is mentioned in
        date is the date the drug was mentioned
    """


    df_drugs = spark.read.csv(
                                df_drugs_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )
    
    df_pubmed = spark.read.csv(
                                df_pubmed_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )
    
    df_clinical = spark.read.csv(
                                df_clinical_path, 
                                header=True, 
                                mode="DROPMALFORMED"
                                )




    cross_join_pubmed_drug = df_drugs.crossJoin(df_pubmed)

    drug_to_pubmed = cross_join_pubmed_drug\
                                    .withColumn('flag_contains', lower(col(dict_column["pubmed"]["title"]))\
                                    .contains(lower(col(dict_column["drug"]["drug"]))))\
                                    .filter(col("flag_contains") == True)\
                                    .select(dict_column["drug"]["atccode"], 
                                                dict_column["drug"]["drug"], 
                                                dict_column["pubmed"]["title"], 
                                                dict_column["pubmed"]["id"], 
                                                dict_column["pubmed"]["date"], 
                                                dict_column["pubmed"]["journal"])
    
    cross_join_clinical_drug = df_drugs.crossJoin(df_clinical)


    drug_to_clinical = cross_join_clinical_drug\
                                    .withColumn('flag_contains', lower(col(dict_column["clinical"]["scientific_title"])).contains(lower(col("drug"))))\
                                    .filter(col("flag_contains") == True)\
                                    .select(dict_column["drug"]["atccode"], 
                                                dict_column["drug"]["drug"], 
                                                dict_column["clinical"]["scientific_title"], 
                                                dict_column["clinical"]["id"], 
                                                dict_column["clinical"]["date"], 
                                                dict_column["clinical"]["journal"])

    drug_to_journal = drug_to_clinical\
                    .union(drug_to_pubmed)\
                    .withColumn("id", sha1("journal")).\
                    select("atccode", "drug", "id", "journal", "date").distinct()
    
    edge_df_pubmed = drug_to_pubmed.select(dict_column["drug"]["atccode"],
                                                dict_column["pubmed"]["id"], 
                                                dict_column["pubmed"]["date"])

    edge_df_clinical = drug_to_clinical.select(dict_column["drug"]["atccode"],
                                                    dict_column["clinical"]["id"], 
                                                    dict_column["clinical"]["date"])
    edge_df_journal = drug_to_journal.select(dict_column["drug"]["atccode"],
                                                    dict_column["clinical"]["id"], 
                                                    dict_column["clinical"]["date"])

    edge_df = edge_df_pubmed.\
                    union(edge_df_clinical).\
                    union(edge_df_journal).\
                    withColumnRenamed("atccode", 'src').\
                    withColumnRenamed("id", 'dst')
    edge_df.write.mode('overwrite').csv(spark_df_output_path, header = 'true')
    return (spark_df_output_path)