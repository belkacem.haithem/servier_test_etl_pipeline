# Path to data Files 
clinical_path = '../data/clinical_trials.csv' 
drugs_path = '../data/drugs.csv'
pubmedCsv_path = '../data/pubmed.csv'
pubmedJson_path = '../data/pubmed.json'



dict_paths = {#### paths of saved intermediary data

    'path_local_pubmed' : '../output/pubmed_df.csv',
    'path_local_clinical' : '../output/clinical_df.csv',
    'path_local_drug' : '../output/drug_df.csv',
    'path_local_pubmed_norm' : '../output/pubmed_df_norm.csv',
    'path_local_clinical_norm' : '../output/clinical_df_norm.csv',
    'path_local_drug_norm' : '../output/drug_df_norm.csv',
    'path_local_vertex' : '../output/vertex_df.csv',
    'path_local_edge' : '../output/edge_df.csv'

}


dict_granularity_clinical_trial = { # Dictionnaries allowing direction in which to fill empty Data in clinical data
                                    'id' : ['scientific_title','date','journal'], 
                                    'scientific_title' : ['date','journal', 'id']
                                }
dict_granularity_pubmed = { # Dictionnaries allowing direction in which to fill empty Data in pubmed data
                            'id' : ['title','date','journal'],
                            'title' : ['date','journal', 'id']
                        }       


dict_column = { # Name of new Columns
                "pubmed": {
                            'id': "id",
                            'title': "name",
                            'date': "date",
                            'journal' : "journal"
                            },
                "clinical": {
                            'id': "id",
                            'scientific_title': "name",
                            'date': "date",
                            'journal' : "journal"
                            },
                "drug" : {
                        "atccode" : "atccode",
                        "drug" : "drug"
                        }
}



dict_type = {   # Name of data Types in aggregated DataFrame
                "pubmed" : "pubmed", 
                "clinical" :  "clinical",      
                "drug" : "drug",
                "journal" : "journal",
                "type" : "type"
                }


output_path = "../output/data.json" # Path to output Files 