SELECT
    date
    , SUM(prod_price * prod_qty) AS ventes
FROM 
    TRANSACTION
WHERE
    date 
        BETWEEN '2019-01-01' AND '2020-01-01'
GROUP BY 
    date