SELECT
    client_id
    , SUM(CASE 
                WHEN product_type = 'MEUBLE' THEN prod_price * prod_qty
                ELSE 0
                END
            ) AS ventes_meuble

    , SUM(CASE 
                WHEN product_type = 'DECO' THEN prod_price * prod_qty
                ELSE 0
                END
            ) AS ventes_deco
FROM 
    TRANSACTION AS TR
    JOIN PRODUCT_NOMENCLATURE AS PN
    ON TR.prod_id = PN.product_id

WHERE
    date 
        BETWEEN '2019-01-01' AND '2020-01-01'
GROUP BY 
    client_id