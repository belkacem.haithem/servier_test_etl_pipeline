# Servier_Test_ETL_Pipeline

Il y 3 Parties :

- Une chaine de traitement permettant de créer un Json représentant un Graph
- Un traitement Ad Hoc permettant d'extraire de l'information du Graph généré
- Des scripts SQL de requette

# Data Pipeline

Celle-ci se lance à partir du script Bash "script.sh" dans le répértoire racine du projet.
Cette pipeline utilise les fichiers contenus dans:

- Data
- python_ETL

Et écris les résultat dans output.

Le nom de toute les variables utilisées est dans ./python_ETL/variables.py
Le script "main" est python_pipeline.py
On retrouve dans les 3 autres fichiers python des fonctions correspondants aux étapes "extract", "transform" et "load".

Le résultat sous forme JSON est écrit dans le dossier output et est constitué de 2 data_frames :

- vertex = qui va venir définir chacunes des nodes du Graph

- edge = qui vient préciser les relations entre les nodes (sources ==> destination)

Une autre façon de lancer la chaine de traitement est en utilisant "etl_dag.py". Ce script orchestre un workflow dans airflow
# Traitement Ad-Hoc

Le traitement ad-Hoc est présent dans un .ipynb (utilisé avec VS code) sotué dans ./python_Adhoc
Il permet d'extraire le journal présentant référençant le plus de drug. Il utilise le fichier JSON produit par la Data Pipeline qu'il va chercher directement à son lieu de création.

# Les fichiers SQL

Ils se situent dans sql_scripts.
Ils sont testés dans un .ipynb situé dans le même dossier.
La base Transaction a été rennomée Transact dans le fichier de test

